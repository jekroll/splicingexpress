#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll 
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

use strict;

my $comando = $ARGV[ 0 ];
my %GENOMES = ();
&load_data;

if ($comando eq 'list') {
  #list all available species and assemblies
  &list_assemblies;
} else {
  #download new data from UCSC
  &download($comando);
}
exit;


### LOAD DATA
sub load_data {
  local $/;

  #list of genome assemblies
  open ASSEM, "./ref_data/assemblies.txt";
  my $data = <ASSEM>;

  while ( $data =~ s/#(.+?)\n((?:\S+?\n)+)#---//m ) {
    my ( $specie, $assem_aux ) = ( $1, $2 );
    chomp $assem_aux;
    my @ASSEMBLIES = split /\n/, $assem_aux;

    foreach my $assem_id ( @ASSEMBLIES ) {
      $GENOMES{ ASSEM }{ lc $assem_id } = [ $assem_id, $specie ];
      push @{ $GENOMES{ SPECIE }{ $specie } }, $assem_id;
    }
  }
  close ASSEM;
}


sub download {
  my $assembly_aux = shift;
  my ( $assembly, $specie ) = @{ $GENOMES{ ASSEM }{ lc $assembly_aux } };

  unless ( $specie ) {
    die "$assembly does not exist! Check the genome assembly id.";
  }

  #write real name
  open ASSEM, ">./tmp/assem.txt";
  print ASSEM $assembly;
  close ASSEM;

  #curl or wget?
  my $curl = `which curl`;
  chomp $curl;

  my $wget = `which wget`;
  chomp $wget;

  ## Download data!
  if ( $curl ) {
    `$curl "ftp://hgdownload.cse.ucsc.edu/goldenPath/$assembly/database/refFlat.txt.gz" -o "refFlat.txt.gz" > /dev/null 2> /dev/null`;
    `$curl "ftp://hgdownload.cse.ucsc.edu/goldenPath/$assembly/database/refSeqAli.txt.gz" -o "refSeqAli.txt.gz" > /dev/null 2> /dev/null`;
  } elsif ($wget) {
    `$wget "ftp://hgdownload.cse.ucsc.edu/goldenPath/$assembly/database/refFlat.txt.gz" > /dev/null 2> /dev/null`;
    `$wget "ftp://hgdownload.cse.ucsc.edu/goldenPath/$assembly/database/refSeqAli.txt.gz" > /dev/null 2> /dev/null`;
  } else {
    die "'curl' and 'wget' not found! Please install one and execute Splicing Express again\n";
  }

}


sub list_assemblies {
  my $std_column = 30;

  foreach my $specie ( sort keys %{ $GENOMES{ SPECIE } } ) {
    my $space = $std_column - length( $specie );
    print "$specie";

    my $line = 0;
    foreach my $assem ( @{ $GENOMES{ SPECIE }{ $specie } } ) {
      $line ++;
      $space = $std_column if ( $line > 1 );
      print ' ' x $space . "$assem\n";
    }
  }
}
