#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll 
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

use strict;

my $analysis_name = $ARGV[ 0 ];
my $assem_id = $ARGV[ 1 ];
my $in_dir = $ARGV[ 2 ];
my %STATS = ();
my %STATS_GLOB = ();

open LISTA, ">$in_dir/index.html";
my @JSON_cache = ();

print LISTA "<!DOCTYPE html>
<html>
<head lang='en'>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <title></title>
    <link rel='stylesheet' href='style/bootstrap.min.css'>
    <link rel='stylesheet' href='style/spex_style.css'>
    <link rel='stylesheet' href='style/jquery-ui.css'>
    <link rel='stylesheet' href='style/ui.jqgrid.css'>

    <style>
      .ui-search-menu {
        position: absolute;
        z-index:99999;
      }
    </style>

</head>
<body>
<nav id='main-header' class='navbar navbar-inverse navbar-static-top'>
    <div class='container'>
        <div class='navbar-header'>
            <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar-collapse'>
                <span class='sr-only'>Toggle navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </button>
            <a class='navbar-brand' href='#'>
                <img src='images/spexp_logo.png' alt='Splice Express'>
                <span class='sr-only'>Splice Express</span>
            </a>
        </div>
        <div class='collapse navbar-collapse' id='navbar-collapse'>
            <ul class='nav navbar-nav'>
                <li class='active'><a href='#'>Explore<span class='sr-only'>(current)</span></a></li>
                <li><a href='stats.html'>Statistics</a></li>
                <li><a href='help.html'>Help</a></li>
            </ul>
        </div>
    </div>
</nav>

<main class='container'>
    <section class='row'>
        <header>
            <h2>$analysis_name</h2>
        </header>

        <div>
        <div class='col-sm-6'>
            <div class='panel panel-primary' style='width:900px'>
                <div class='panel-heading'>
                    <h3>Gene Expression data</h3>
                </div>
            
                <table id='jqGrid' class='table display'></table>
                <div id='jqGridPager'></div>\n";
            

foreach my $letter ( '0-9', 'A'..'Z' ) {
  my $letter_aux = $letter;

  if ( $letter eq '0-9' ) {
    $letter_aux = "NUM";
  }

  if ( -d "$in_dir/$letter" ) {
    my @GENES = `cd $in_dir/$letter; ls -1`;
    map { chomp } @GENES;
    $STATS_GLOB{ TOTAL } = scalar @GENES;

    foreach my $gene ( @GENES ) {
      my $event_count = 0;
      my @EVENT_tags = ();

      my %DESC = ();
      foreach my $AS ( `cat $in_dir/$letter/$gene/STAT.txt` ) {
        my ( $type, $count ) = $AS =~ /^(\S+)\s+(\S+)$/i;
        next unless ( $type );
        $DESC{ $type } = $count;
      }

      unless (exists $DESC{'MEDIAN'} || $DESC{'MEDIAN'}) {
        $DESC{ 'MEDIAN' } = 0;
      }

      my $median_round = $DESC{ 'MEDIAN' } + ( 100 - $DESC{ 'MEDIAN' } % 100 );
      $median_round = 1000 if ( $median_round >= 1000 );
      $STATS_GLOB{ $median_round } ++; 

      foreach my $type ( 'SKIP', 'ALT3', 'ALT5', 'RETENT' ) {
        if ( exists $DESC{ $type } ) {
          $DESC{ $type } = 0 unless ( $DESC{ $type } );
          $STATS{ $type }{ TOTAL } += $DESC{ $type };
          $STATS{ $type }{ GENES } ++;
        } else {
          $DESC{ $type } = 0;
        }
      }

      #do not create links for genes without expression
      my $gene_content = '';
      if ( -e "$in_dir/$letter/$gene/gene_$gene.svg" ) {
        my $json_id = $#JSON_cache + 1;
        my $aux_json = "{id: $json_id, gene: \"$gene\", exp: $DESC{'MEDIAN'}, skp: $DESC{'SKIP'}, alt3: $DESC{'ALT3'}, alt5: $DESC{'ALT5'}, ret: $DESC{'RETENT'}}";
        push @JSON_cache, $aux_json;

        $gene_content = `cat $in_dir/$letter/$gene/gene_$gene.svg.report`;
        open PAGEGEN, ">$in_dir/$letter/$gene/$gene.html";

      } else {
        $STATS_GLOB{ NOEXP }++;
        next;
      }

      ############################################
      ####### Create page for gene
      my @EVENTS = `cd $in_dir/$letter/$gene; ls -1 ./CLUST_$gene\_*.svg 2> /dev/null`;
      map { chomp } @EVENTS;

      print PAGEGEN "<!DOCTYPE html>
<html>
<head lang='en'>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>

  <title></title>

  <!-- Stylesheet -->
  <link rel='stylesheet' href='../../style/bootstrap.min.css'>
  <link rel='stylesheet' href='../../style/spex_style.css'>
  <link rel='stylesheet' href='../../style/tablesorter.css'>

</head>
<body>
<nav id='main-header' class='navbar navbar-inverse navbar-static-top'>
  <div class='container'>
    <div class='navbar-header'>
      <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar-collapse'>
        <span class='sr-only'>Toggle navigation</span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
      </button>
      <a class='navbar-brand' href='#'>
        <img src='../../images/spexp_logo.png' alt='Splice Express'>
        <span class='sr-only'>Splice Express</span>
      </a>
    </div>
    <div class='collapse navbar-collapse' id='navbar-collapse'>
      <ul class='nav navbar-nav'>
        <li><a href='../../index.html'>Explore</a></li>
        <li><a href='../../stats.html'>Statistics</a></li>
        <li><a href='../../help.html'>Help</a></li>
      </ul>
    </div>
  </div>
</nav>

<main class='container'>
  <header>
    <h2>Gene '$gene' splice profile</h2>
  </header>

  <section id='exp' class='row'>
    <h3>Gene expression</h3>
    <div class='col-sm-6'>
      <img src='gene_$gene.svg' class='img-responsive'>
    </div>

    $gene_content
  </section>\n";

      if ( @EVENTS ) {
        print PAGEGEN "  <section id='alternative'class='row'>
  <h3>Alternative Splice Expression</h3>\n";

        my %ORDER = ();
        foreach my $graph_as ( @EVENTS ) {
          my ( $chr, $start, $end ) = $graph_as =~ /_([^_]+)_(\d+):(\d+)\.svg$/i;
          unless ( $start || $end ) {
            die "error??? $graph_as\n";
          }
          push @{ $ORDER{ $start } }, "$graph_as,$chr,$start,$end";
        }

        foreach my $as_start ( sort { $a <=> $b } keys %ORDER ) {
          foreach my $as_images ( @{ $ORDER{ $as_start } } ) {
            $event_count++;
            push @EVENT_tags, "ASE$event_count";

            my ( $graph_as, $chr, $start, $end ) = split /,/, $as_images;
            my $percent = int ( 0.2 * ( 1 + $end - $start ) );
            $start -= $percent;
            $end += $percent;

            my $report_content = `cat $in_dir/$letter/$gene/$graph_as.report`;
            $report_content =~ s/<<ASE_COUNT>>/ASE$event_count/img;
            $report_content =~ s/<<AS_GRAPH>>/$graph_as/im;
            print PAGEGEN $report_content;
          }
        }
      }

      print PAGEGEN "</section>
</main>

<br><br><br><br>
<footer id='main-footer' class='navbar navbar-fixed-bottom'>
  <div clas='container'>
    <div class='col-sm-4'>
      <p>A Suite for Alternative Splicing Analysis Using Data from Next-Generation Sequencing. For more info checkout our page <a href='http://www.bioinformatics-brazil.org/splicingexpress/' target='_blank'>here</a>.</p>
      <p>Copyright (c) 2015 Kroll, J.E. et al</p>
    </div>
  </div>
</footer>

<!-- Javascript import -->
<script src='../../js/jquery.min.js'></script>
<script src='../../js/bootstrap.min.js'></script>
<script src='../../js/jquery.tablesorter.min.js'></script>
<script>
   \$(document).ready(function() 
   {\n";

      #run tablesorter
      foreach my $html_event_tag ( @EVENT_tags ) {
        print PAGEGEN "      \$('#t$html_event_tag').tablesorter({sortList: [[5,0]]});\n";
      }
      print PAGEGEN "      \$('#gene_exp').tablesorter({sortList: [[1,0]]});\n";
      print PAGEGEN "   });
</script>
</body>
</html>\n";

      close PAGEGEN;
    }
  }
}


#open JSON, ">$in_dir/results.json";
#print JSON "{\"rows\":[" . join( ",\n", @JSON_cache ) . "]}";
my $json_content = "var json = [" . join( ",\n", @JSON_cache ) . "];";
#close JSON;


print LISTA "            </div>
        </div>
        </div>
    </section>
</main>

<br><br><br><br>
<footer id='main-footer' class='navbar navbar-fixed-bottom'>
  <div clas='container'>
    <div class='col-sm-4'>
      <p>A Suite for Alternative Splicing Analysis Using Data from Next-Generation Sequencing. For more info checkout our page <a href='http://www.bioinformatics-brazil.org/splicingexpress/' target='_blank'>here</a>.</p>
      <p>Copyright (c) 2015 Kroll, J.E. et al</p>
    </div>
  </div>
</footer>

<!-- Javascript import -->
<script src='js/jquery.min.js'></script>
<script src='js/bootstrap.min.js'></script>
<script src='js/grid.locale-en.js'></script>
<script src='js/jquery.jqGrid.min.js'></script>

<script type='text/javascript'>
      function addLink(cellvalue, options, rowObject) {
        return \"<a href='./\"+cellvalue.charAt(0)+\"/\"+cellvalue+\"/\"+cellvalue+\".html' target='_blank' style='color: blue'>\"+cellvalue+\"</a>\";
      }

     \$(document).ready(function () {
         $json_content
         
         \$('#jqGrid').jqGrid({
             datatype: 'local',
             data: json,
             loadonce: true,
             ignoreCase: true,
             gridview: true,
             colModel: [
               { label: 'Gene', name: 'gene', width: 150, sorttype:'string', formatter: addLink, searchoptions: {sopt:['eq','bw','bn','cn','nc','ew','en']}},
               { label: 'Median Expression', name: 'exp', width: 150, sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
               { label: '# Exon Skipping', name: 'skp', width: 150, sorttype:'integer',searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
               { label: '# Alt. \\'3 Splice border', name: 'alt3', width: 150, sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
               { label: '# Alt. \\'5 Splice border', name: 'alt5', width: 150, sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}},
               { label: '# Intron Retention', name: 'ret', width: 150, sorttype:'integer', searchoptions:{sopt:['eq','ne','le','lt','gt','ge']}}
             ],
             viewrecords: true,
             width: 900,
             height: 'auto',
             rowNum: 30,
             pager: '#jqGridPager',
         }).jqGrid('filterToolbar',{
            searchOperators : true
         });
      });
</script>

</body>
</html>\n";



close LISTA;

#### PRINT STATS
open STATSh, ">$in_dir/stats.html";
print STATSh "<!DOCTYPE html>
<html>
<head lang='en'>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>

  <title></title>

  <!-- Stylesheet -->
  <link rel='stylesheet' href='style/bootstrap.min.css'>
  <link rel='stylesheet' href='style/spex_style.css'>
  <link rel='stylesheet' href='style/tablesorter.css'>

</head>
<body>
<nav id='main-header' class='navbar navbar-inverse navbar-static-top'>
  <div class='container'>
    <div class='navbar-header'>
      <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#navbar-collapse'>
        <span class='sr-only'>Toggle navigation</span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
      </button>
      <a class='navbar-brand' href='#'>
        <img src='images/spexp_logo.png' alt='Splice Express'>
        <span class='sr-only'>Splice Express</span>
      </a>
    </div>
    <div class='collapse navbar-collapse' id='navbar-collapse'>
      <ul class='nav navbar-nav'>
        <li><a href='index.html'>Explore</a></li>
        <li class='active'><a href='#'>Statistics<span class='sr-only'>(current)</span></a></li>
        <li><a href='help.html'>Help</a></li>
      </ul>
    </div>
  </div>
</nav>

<main class='container'>
  <header>
    <h2>Basic Statistics</h2>
  </header>
    <section id='exp' class='row'>
        <h3>Events Frequency</h3>
    </section>
    <table class='table'>
    <thead>
      <tr>
        <th>Event type</th>
        <th># Genes</th>
        <th># Total Events</th>
        <th>Events / Gene</th>
      </tr>
    </thead>
    <tbody>\n";

my %AS_FULLNAMES = (
  'ALT5' => "Alternative 5\' Splice Site",
  'ALT3' => "Alternative 3\' Splice Site",
  'RETENT' => "Intron Retention",
  'SKIP' => "Exon Skipping"
);

foreach my $type ( 'SKIP', 'ALT3', 'ALT5', 'RETENT' ) {
  my $genes = $STATS{ $type }{ GENES };
  my $events = $STATS{ $type }{ TOTAL };
  my $events_per_gene = sprintf( "%.2f", $events / $genes );
  my $AS_type = $AS_FULLNAMES{ $type };
 
  print STATSh "        <tr><th>$AS_type</th><th>$genes</th><th>$events</th><th>$events_per_gene</th></tr>\n";
}

print STATSh "    </tbody>
    </table>
</main>

<br><br><br><br>
<footer id='main-footer' class='navbar navbar-fixed-bottom'>
  <div clas='container'>
    <div class='col-sm-4'>
      <p>A Suite for Alternative Splicing Analysis Using Data from Next-Generation Sequencing. For more info checkout our page <a href='http://www.bioinformatics-brazil.org/splicingexpress/' target='_blank'>here</a>.</p>
      <p>Copyright (c) 2015 Kroll, J.E. et al</p>
    </div>
  </div>
</footer>";

close STATSh;

